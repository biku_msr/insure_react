import React, { Component } from 'react';
import {connect} from 'react-redux';

export class InsuranceDeal extends Component{

    renderInsuranceDeal(insurances){
        return insurances.map((insrance,i) => {
            return (
              <div key={i}>
                {insrance.insuranceName}
              </div>

            );
          })
    }

    render(){
        if(!this.props.deal){
            return(
                <div>
                    <h2>Click on check</h2>
                </div>
            )
        }
        return(
            <div>
                {/* <h1>{this.props.user.name}</h1> */}
                {this.renderInsuranceDeal(this.props.deal)}
            </div>
        );
    }
}

function mapStateToProps(state){
    return {
      user: state.action,
      deal: state.insurance
    }
}

export default connect(mapStateToProps)(InsuranceDeal);