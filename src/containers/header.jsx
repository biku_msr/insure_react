import React, { Component } from 'react';
import {connect} from 'react-redux';

import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import FlatButton from 'material-ui/FlatButton';
// import Toggle from 'material-ui/Toggle';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
// import NavigationClose from 'material-ui/svg-icons/navigation/close';
import { bindActionCreators } from 'redux';

import {login} from '../action/index';
import {logout} from '../action/index';


// class Login extends Component {
//     static muiName = 'FlatButton';
  
//     render() {
//       return (
//         <FlatButton {...this.props} label="Login" onClick={() => this.props.login()}/>
//       );
//     }
//   }
  
//   const Logged = (props) => (
//     <IconMenu
//       {...props}
//       iconButtonElement={
//         <IconButton><MoreVertIcon /></IconButton>
//       }
//       targetOrigin={{horizontal: 'right', vertical: 'top'}}
//       anchorOrigin={{horizontal: 'right', vertical: 'top'}}
//     >
//       <MenuItem primaryText="Refresh" />
//       <MenuItem primaryText="Help" />
//       <MenuItem primaryText="Sign out" onClick={() => this.props.logout()}/>
//     </IconMenu>
//   );
  
//   Logged.muiName = 'IconMenu';


class Header extends Component {

    state = {
        logged: true,
    };
    
    handleClick = () => {
        alert('onClick triggered on the title component');
    }

    loginButtion = () => {
        return (<FlatButton label="Login" onClick={() => this.props.login()}/>);
    }

    logedInButttion = () => {
        return(
            <IconMenu
            iconButtonElement={
              <IconButton><MoreVertIcon /></IconButton>
            }
            targetOrigin={{horizontal: 'right', vertical: 'top'}}
            anchorOrigin={{horizontal: 'right', vertical: 'top'}}
          >
            <MenuItem primaryText="Refresh" />
            <MenuItem primaryText="Help" />
            <MenuItem primaryText="Sign out" onClick={() => this.props.logout()}/>
          </IconMenu>
        );
    }

    render() {
        return(
            
            <AppBar
            title="Insure"
            iconElementLeft={<IconButton></IconButton>}
            iconElementRight={this.props.auth || localStorage.getItem('USER') ? this.logedInButttion() : this.loginButtion()}
          />
        )
    }
}
function mapStateToProps(state){
    return {
        auth: state.auth
    }
}

function mapDispachToProps(dispatch){
    return bindActionCreators({login: login, logout: logout}, dispatch);
  }
export default connect(mapStateToProps, mapDispachToProps)(Header);