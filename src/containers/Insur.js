import React, { Component } from 'react';

import RaisedButton from 'material-ui/RaisedButton';

import logo from '../logo.svg';
import '../App.css';
import {connect} from 'react-redux';
import {selectUser} from '../action/index';
import {getDeals} from '../action/index';
import { bindActionCreators } from 'redux';

class Insur extends Component {

  createListItem(){
    return this.props.users.map((user,i) => {
      return (
        <li key={i} onClick={() => this.props.selectUser(user)}>{user.name}</li>
      );
    })
  }

  check(){
    alert('hi');
  }
  
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
          The insurance for all.
        </p>
        <ul>
          {this.createListItem()}
        </ul>
        <RaisedButton label="Check" primary={true} onClick={() => this.props.getDeals('http://localhost:8088/fetch/insurance/31-35/0')} />
      </div>
    );
  }
}

function mapStateToProps(state){
  return {
    users: state.users
  }
}

function mapDispachToProps(dispatch){
  return bindActionCreators({selectUser: selectUser, getDeals: getDeals}, dispatch);
}

export default connect(mapStateToProps, mapDispachToProps)(Insur);
