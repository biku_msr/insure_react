import React from 'react';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import Insur from '../containers/Insur'
import InsuranceDeal from '../containers/InsuraceDeals';
import Header from '../containers/header'

const App = () => (
    <MuiThemeProvider>
    <div>
        <Header/>
        <Insur/>
        <hr/>
        <InsuranceDeal/>
    </div>
    </MuiThemeProvider>
);

export default App;