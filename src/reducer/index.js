import {combineReducers} from 'redux';

import UserReducer from './reducer-user';
import ResuserAction from './redicer-action';
import Insurance from './insutance-details';
import UserAuth from './user-auth';

const allReducers = combineReducers({
    users: UserReducer,
    action: ResuserAction,
    insurance: Insurance,
    auth: UserAuth
});

export default allReducers;