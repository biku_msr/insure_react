export default function(state = null, action){
    switch(action.type){
        case "LOGIN":
            console.log("login");
           return action.payload;
        case "LOGOUT":
            console.log("logout");
            localStorage.clear();
            return action.payload;
        default:
            break;
    }
    return state
}