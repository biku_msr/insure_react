import axios from 'axios';

export const selectUser = (user) => {
    console.log(user.name);
    return{
        type: "USER_SELECTED",
        payload: user
    }
}

export const getDeals = (url) => {
    console.log(url);
    return(dispatch) => {
        return axios.get(url).then((res)=>{
            dispatch(
                {
                    type: "INSURANCE",
                    payload: res.data
                }
            )
        })
    }
}

export const login = () => {
    console.log("loging in ");
    return(dispatch) => {
        localStorage.setItem("USER", true);
        return (
            dispatch(
                {
                    type: "LOGIN",
                    payload: true
                }
            )
        )
    }
}

export const logout = () => {
    console.log("loging out");
    return(dispatch) => {
        localStorage.clear();
        return (
            dispatch(
                {
                    type: "LOGOUT",
                    payload: false
                }
            )
        )
    }
}



